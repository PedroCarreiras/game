extends Node2D

var shards = preload("res://Scenes/Shards.tscn")

var type = "weapon"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Area2D_body_entered(body):
	if body.name == "Player":
		if body.equip(shards.instance()):
			queue_free()
			
