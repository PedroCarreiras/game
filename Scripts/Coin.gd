extends Area2D

var amount = 10

func _on_Coin_body_entered(body):
	if body.get_name() == 'Player':
		if body.add_coins(amount):
			queue_free()
