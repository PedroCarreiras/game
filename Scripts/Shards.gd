extends Node2D

var type = "weapon"

var ammoScene = load("res://Scenes/ShardProjectile.tscn")
export var maxAmmo = 5
var magazine = []

func _ready():
	$Reload.start()

func fire():
	if $Reload.is_stopped() and len(magazine) < maxAmmo:
		$Reload.start()
	if len(magazine) > 0:
		magazine[0].fire()
		magazine.remove(0)

func equip(ammo):
	for i in range(ammo):
		var ammoInstance = ammoScene.instance()
		ammoInstance.get_child(0).position = Vector2(0, -200)
		ammoInstance.rotation_degrees = len(magazine)*360/maxAmmo
		magazine.append(ammoInstance)
		add_child(ammoInstance)

func drop():
	var shardsItem = load("res://RefactoredScenes/Pick-ups/Equipment/Weapons/Shards.tscn").instance()
	shardsItem.maxAmmo = maxAmmo
	shardsItem.ammo = len(magazine)
	for i in magazine:
		i.queue_free()
	shardsItem.position = global_position + (get_global_mouse_position()-global_position).normalized() * 100
	get_parent().get_parent().add_child(shardsItem)

func _on_Reload_timeout():
	if len(magazine) < maxAmmo:
		var ammoInstance = ammoScene.instance()
		ammoInstance.get_child(0).position = Vector2(0, -200)
		ammoInstance.rotation_degrees = len(magazine)*360/maxAmmo
		magazine.append(ammoInstance)
		add_child(ammoInstance)
	else:
		$Reload.stop()
	
