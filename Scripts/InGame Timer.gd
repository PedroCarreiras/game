extends Label

var time = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	set_text(str(time))

func _on_Timer_timeout():
	time += 1
