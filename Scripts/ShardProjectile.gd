extends Node2D

var speed = 500
var direction = Vector2(0,0)

var bullet = preload("res://Scenes/Bullet.tscn")

func _ready():
	pass

func _process(delta):
	pass

func fire():
	var bulletInstance = bullet.instance()
	bulletInstance.position = $Sprite.global_position
	get_tree().get_root().get_child(0).add_child(bulletInstance)
	queue_free()
