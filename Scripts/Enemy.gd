extends Node2D

func _on_Area2D_body_entered(body):
	if "Bullet" in body.name:
		body.queue_free()
	queue_free()
