extends Control

var location = Vector2(0, 0)
var time = .8

func _ready():
	var tween = get_node("Tween")
	tween.interpolate_property($Label, "rect_position", location, location - Vector2(0, 50), time, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	tween.interpolate_property($Label, "modulate", Color(1,1,1,1), Color(1,1,1,0), time, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	tween.start()

func _on_Tween_tween_all_completed():
	queue_free()
