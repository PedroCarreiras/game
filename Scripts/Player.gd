extends KinematicBody2D

var velocity = Vector2(0, 0)
var speed = 200
var isBoosting = false

var coins = 0
var coinsMax = 100

var bullet_scene = preload("res://Scenes/Bullet.tscn")

var cash_scene = preload("res://Scenes/Cash.tscn")

var equipmentList = []

var has_weapon = false

func _ready():
	pass

func _process(delta):
	look_at(get_global_mouse_position())
	
	velocity = Vector2(0, 0)
	if Input.is_action_pressed("move_up"):
		velocity.y = -1
	if Input.is_action_pressed("move_down"):
		velocity.y = 1
	if Input.is_action_pressed("move_right"):
		velocity.x = 1
	if Input.is_action_pressed("move_left"):
		velocity.x = -1
		
	velocity = velocity.normalized() * speed * delta

	if isBoosting:
		if $BoostTimer.get_time_left() == 0:
			speed_boost()
		velocity *= 2
		
	position += velocity
	
#	if Input.is_action_pressed("shoot"):
#		fire()
	
func _input(event):
	if event.is_action_pressed("ability_boost") and not isBoosting:
		speed_boost()
	if event.is_action_pressed("camera_lock"):
		camera_lock()
	if event.is_action_pressed("shoot"):
		fire()
	if event.is_action_pressed("drop"):
		drop()

func speed_boost():
	$BoostTimer.start(3)
	isBoosting = not isBoosting

func camera_lock():
	$Camera2D.current = not $Camera2D.current

func add_coins(amount):
	var cash = cash_scene.instance()
	if 1:#coins + amount < coinsMax:
		coins += amount
		cash.get_child(0).text = "+ " + str(amount)
		cash.location = position - Vector2(cash.get_child(0).rect_size.x/2, 50)
		get_parent().add_child(cash)
		return true
	else:
		return false

func fire():
	if has_weapon:
		if get_child(4).get_child(0).paused:
			get_child(4).get_child(0).start()
		if len(get_child(4).magazine) > 0:
			get_child(4).magazine[0].fire()
			get_child(4).magazine.remove(0)
		pass
	else:
		var bullet = bullet_scene.instance()
		bullet.position = global_position
		get_parent().add_child(bullet)

func equip(equipment):
	add_child(equipment)
	has_weapon = true

func drop():
	if len(equipmentList) > 0:
		var droppedItem = load(equipmentList[0]).instance()
		droppedItem.position = global_position + (get_global_mouse_position()-global_position).normalized() * 100
		get_parent().add_child(droppedItem)
		equipmentList.remove(0)
		has_weapon = false
		get_child(4).clear_magazine()
		get_child(4).queue_free()
	
