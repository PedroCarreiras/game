extends Control

var coin = preload("res://RefactoredScenes/Pick-ups/Valuables/Coin.tscn")

func _ready():
	var coinInstance = coin.instance()
	coinInstance.scale = Vector2(2,2)
	$CenterContainer.add_child(coinInstance)

func _process(delta):
#	update_progress(progress)
	pass

func update_progress(progress : float) -> void: 
	$VBoxContainer/ProgressBar.value.set_value(progress)
