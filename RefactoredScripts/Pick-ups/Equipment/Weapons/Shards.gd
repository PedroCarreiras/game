extends Node2D

var type = "weapon"

var ammoScene = preload("res://Scenes/ShardProjectile.tscn")
export var maxAmmo = 5
var ammo = 0

var shards = load("res://Scenes/Shards.tscn")

func _ready():
	pass

func _process(delta):
	pass

func _on_Area2D_body_entered(body):
	if body.name == "Player":
		var shardsInstance = shards.instance()
		shardsInstance.maxAmmo = maxAmmo
		if body.equip(shardsInstance):
			shardsInstance.equip(ammo)
			queue_free()
