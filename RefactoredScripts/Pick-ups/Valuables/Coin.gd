tool
extends Node2D

var rng = RandomNumberGenerator.new()
export var random = true

enum VALUE {Silver=1, Gold=10, Ruby=50}
export(VALUE) var value

func _ready():
	rng.randomize()
	value = VALUE.values()[rng.randi_range(0, 2)]
	$AnimatedSprite.play(str(value))

func _on_Coin_body_entered(body):
	if body.get_name() == 'Player':
		if body.add_coins(value):
			queue_free()

func _on_AnimatedSprite_animation_finished():
	if random:
		rng.randomize()
		value = VALUE.values()[rng.randi_range(0, 2)]
	$AnimatedSprite.play(str(value))
