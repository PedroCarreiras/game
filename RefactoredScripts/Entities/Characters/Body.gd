extends KinematicBody2D

func _ready():
	pass

func _process(delta):
	look_at(get_global_mouse_position())

func _input(event):
	if event.is_action_pressed("camera_lock"):
		camera_lock()

func camera_lock():
	$Camera2D.current = not $Camera2D.current

func equip(object):
	return get_parent().equip(object)

func add_coins(amount):
	$AudioStreamPlayer.play()
	return get_parent().add_coins(amount)
