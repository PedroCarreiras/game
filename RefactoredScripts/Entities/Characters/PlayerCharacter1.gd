extends Node2D

var equipment = {"torso": null, "head":null, "feet":null, "weapon":null}

var spells = []

var velocity:Vector2
export var speed = 200

var coins = 0
var coinsMax = 100
var cash_scene = preload("res://Scenes/Cash.tscn")

func _ready():
	pass

func _process(delta):
	move(delta)

func _input(event):
	if event.is_action_pressed("shoot"):
		fire()
	if event.is_action_pressed("interact"):
		pass
	if event.is_action_pressed("drop"):
		drop(equipment["weapon"])

func move(delta):
	velocity = Vector2(0, 0)
	if Input.is_action_pressed("move_up"):
		velocity.y = -1
	if Input.is_action_pressed("move_down"):
		velocity.y = 1
	if Input.is_action_pressed("move_right"):
		velocity.x = 1
	if Input.is_action_pressed("move_left"):
		velocity.x = -1
		
	velocity = velocity.normalized() * speed * delta
	position += velocity

func fire():
	if equipment["weapon"]:
		equipment["weapon"].fire()

func equip(object):
	if not equipment[object.type]:
		equipment[object.type] = object
		add_child(object)
		return true
	return false

func drop(object):
	if object:
		object.drop()
		remove_child(object)
		equipment[object.type] = null

func add_coins(amount):
	var cash = cash_scene.instance()
	if 1:#coins + amount < coinsMax:
		coins += amount
		cash.get_child(0).text = "+ " + str(amount)
		cash.location = position - Vector2(cash.get_child(0).rect_size.x/2, 50)
		get_parent().add_child(cash)
		return true
	else:
		return false
